from __future__ import division
import sys,glob,argparse,copy,operator
from nltk.stem import WordNetLemmatizer as WNL
from collections import Counter as C

parser = argparse.ArgumentParser(description="Evaluate a summary against references using variations over ROUGE-1.")
parser.add_argument('summary',help='System predicted summary')
parser.add_argument('references',help='A prefix common to the reference summaries, parsed by glob')
parser.add_argument('--lemmatize',help='Use the Princeton Wordnet lemmatizer',action="store_true",default=False)
parser.add_argument('--corrected',help='Use chance-corrected ROUGE-1',action="store_true",default=False)
parser.add_argument('--remove_stopwords',help='Remove stopwords',action="store_true",default=False)
parser.add_argument('--bigrams',help='Use ROUGE-2',action="store_true",default=False)
parser.add_argument('--balanced',help='Balanced (F1-like) ROUGE-n',action="store_true",default=False)
parser.add_argument('--word_trunc',help='Truncate at 100 words (rather than 665 bytes)',action="store_true",default=False)
args=parser.parse_args()

name=sys.argv[1].split("/")[-1].split(".")[0]

files=glob.glob(sys.argv[2]+"*.sent.tok")
F=len(files)
wnl=WNL()

def bigramify(low): # list of words
    new_low=[]
    for i in range(len(low))[1:]:
        new_low.append(low[i-1]+"_"+low[i])
    return new_low

for i in range(4):
    if args.word_trunc:
        summary=open(sys.argv[1]).read().lower().strip().split()[:100] # list of words
        references=[open(f).read().lower().strip().split() for f in [files[i]]] # list of lists of words
    else:
        summary=open(sys.argv[1]).read().lower()[:665].strip().split() # list of words
        references=[open(f).read().lower().strip().split() for f in [files[i]]]


    references=[item for sublist in references for item in sublist] # list of words

    if args.lemmatize:
        summary=[wnl.lemmatize(w) for w in summary]
        for r in references:
            r=[wnl.lemmatize(w) for w in r]
        
    if args.remove_stopwords: # this does not work right now
        stopwords=open("english").read().strip().split()
        vocab=copy.deepcopy(R)
        for w in vocab:
            if w in stopwords:
                del R[w]

    if args.bigrams:
        references=bigramify(references)
        summary=bigramify(summary)
        
    R=C(references)
    S=C(summary)
    shared=S&R # intersection

    if args.corrected:
#        print sorted(R.items(), key=operator.itemgetter(1),reverse=True)[:10]
        text=open("docs/all_"+name+".sent.tok").read().strip().split()
        if args.lemmatize:
            text=[wnl.lemmatize(w) for w in text]
        if args.bigrams:
            text=bigramify(text)
        T=C(text)
        for w in R:
            if w not in T:
                T[w]=1
            R[w]=R[w]/T[w]
#        print sorted(R.items(), key=operator.itemgetter(1),reverse=True)[:10]
        for w in shared:
            shared[w]=shared[w]/T[w]

    if args.balanced:
        rouge=2*sum(shared.values())/(sum(R.values())+sum(S.values()))
    else:
        rouge=sum(shared.values())/sum(R.values())
    print rouge
    R.clear(), shared.clear(), S.clear()
