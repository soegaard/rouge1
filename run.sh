for OPTS in "" "--corrected" "--balanced" "--corrected --balanced" "--bigrams" ; do #"--balanced" "--balanced --corrected" ; do #"--lemmatize" "--lemmatize --corrected"; do #"" "--corrected" "--lemmatize" "--lemmatize --corrected" "--remove_stopwords"  "--corrected --remove_stopwords" "--lemmatize --remove_stopwords" "--lemmatize --remove_stopwords --corrected"; do
    echo $OPTS
    for sys in ours ; do #ours-corrected ours-corrected-lemmatized ; do #ours-stopwords ; do
	echo $sys
	rm results/$sys.txt
	for d in docs/*.sent; do
	    name=${d:9:7}
	    python2.7 rouge.py SOAsums/$sys/$name.$sys goldsums/sum_$name"_" --word_trunc $OPTS >> results/$sys.txt
	done
	python2.7 print_sum.py results/$sys.txt
    done
    for sys in Centroid FreqSum GreedyKL LexRank TsSum ; do
	echo $sys
	rm results/$sys.txt
	for d in docs/*.sent; do
	    name=${d:9:7}
	    python2.7 rouge.py basicsums/$sys/$name.$sys goldsums/sum_$name"_" --word_trunc $OPTS >> results/$sys.txt
	done
	python2.7 print_sum.py results/$sys.txt
    done
    for sys in CLASSY04 CLASSY11 DPP ICSISumm OCCAMS_V RegSum Submodular ; do
	echo $sys
	rm results/$sys.txt
	for d in docs/*.sent; do
	    name=${d:9:7}
	    python2.7 rouge.py SOAsums/$sys/$name.$sys goldsums/sum_$name"_" --word_trunc $OPTS >> results/$sys.txt
	done
	python2.7 print_sum.py results/$sys.txt
    done
done
